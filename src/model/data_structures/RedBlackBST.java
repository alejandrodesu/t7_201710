package model.data_structures;

import java.time.chrono.MinguoChronology;
import java.util.NoSuchElementException;

public class RedBlackBST<Key extends Comparable<Key>, Value >{
	private static final boolean RED = true; 
	private static final boolean BLACK = false; 
	private NodoRB root; 
	/**
	 * ---------------NODO----------------------
	 */
	private class NodoRB{
		private Key key; 
		private Value val; 
		private NodoRB left, right; 
		private boolean color; 
		private int size; 
		public NodoRB(Key key, Value val, boolean color,int size){
			this.key = key; 
			this.val = val; 
			this.color = color; 
			this.size = size; 
		}
	}
	/**
	 * -------------- Metodo constructor---------------
	 */
	public RedBlackBST(){
	}
	private boolean isRed(NodoRB x){
		if(x == null) return false; 
		return x.color == RED; 
	}
	/**
	 *--------------- Metodos Auxiliares----------------
	 */
	private int size(NodoRB x){
		if (x==null) return 0; 
		return x.size; 

	}
	public int size(){
		return size(root);
	}
	public boolean isEmpty(){
		return root == null; 
	}
	public Value get(Key key){
		if (key == null) throw new IllegalArgumentException("no hay valor"); 
		return get(root, key); 
	}
	private Value get(NodoRB x, Key key){
		while(x != null){
			int cmp = key.compareTo(x.key); 
			if (cmp < 0) x = x.left; 
			else if (cmp > 0) x = x.right; 
			else return x.val;
		}
		return null; 
	}
	public boolean contains(Key key){
		return get(key) != null; 
	}
	/**
	 * ----------------INSERCIONES---------------
	 */
	public void put(Key key, Value val){
		if (key == null) throw new IllegalArgumentException("el primero es null"); 
		if (val == null){
			delete(key); 
			return; 
		}
	}
	private NodoRB put(NodoRB h, Key key, Value val){
		if (h==null) return new NodoRB(key, val, RED, 1); 


		int cmp = key.compareTo(h.key); 
		if (cmp < 0) h.left = put(h.left, key, val); 
		else if (cmp > 0) h.right = put(h.right, key, val); 
		else h.val = val; 


		if (isRed(h.right) && !isRed(h.left)) h = rotateLeft(h); 
		if (isRed(h.left) && isRed(h.left.left)) h =rotateRight(h); 
		if (isRed(h.left) && isRed(h.right)) flipColors(h); 
		h.size = size(h.left) + size(h.right) + 1; 
		return h; 
	}
	/**
	 * ---------------ELIMINAR----------------------------------
	 */
	public void deleteMin(){
		if (isEmpty()) throw new NoSuchElementException("BST sin flujo"); 
		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED; 
		root = deleteMin(root);
		if (!isEmpty()) root.color = BLACK; 
	}
	private NodoRB deleteMin(NodoRB h){
		if(h.left == null)
			return null; 
		if(!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h); 
		h.left = deleteMin(h.left); 
		return balance(h); 
	}
	public void deleteMax(){
		if(isEmpty()) throw new NoSuchElementException("BST sin flujo"); 
		if(!isRed(root.left) && !isRed(root.right)); 
		root.color = RED; 
		root = deleteMax(root);
		if(!isEmpty()) root.color = BLACK; 
	}
	private NodoRB deleteMax(NodoRB h){
		if(isRed(h.left))
			h = rotateRight(h); 
		if(h.right == null)
			return null; 
		if(!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h); 
		h.right = deleteMax(h.right); 
		return balance(h); 
	}
	public void delete(Key key){
		if(key == null) throw new IllegalArgumentException("no hay llave");
		if(!contains(key)) return; 
		if(!isRed(root.left) && !isRed(root.right))
			root.color= RED; 
		root = delete(root, key); 
		if(!isEmpty()) root.color = BLACK; 
	}
	private NodoRB delete(NodoRB h, Key key){
		if(key.compareTo(h.key)<0){
			if(!isRed(h.left) && !isRed(h.left.left))
				h= moveRedLeft(h); 
			h.left = delete(h.left, key); 
		}
		else{
			if(isRed(h.left))
				h = rotateRight(h); 
			if(key.compareTo(h.key) == 0 && (h.right == null))
				return null; 
			if(!isRed(h.right) && !isRed(h.left.left))
				h = moveRedRight(h); 
			if(key.compareTo(h.key) == 0 ){
				NodoRB x = min(h.right); 
				h.key = x.key; 
				h.val = x.val; 
				h.right = deleteMin(h.right); 
			}
			else h.right = delete(h.right, key); 
		}
		return balance(h); 
	}
	/**
	 * ------------------Metodos complementarios-------------------------
	 */
	private NodoRB rotateRight(NodoRB h){
		NodoRB x = h.left; 
		h.left = x.right; 
		x.right = h; 
		x.color = x.right.color;
		x.right.color = RED; 
		x.size = h.size; 
		h.size = size(h.left) + size(h.right) + 1; 
		return x; 
	}
	private NodoRB rotateLeft(NodoRB h){
		NodoRB x = h.right; 
		h.right = x.left; 
		x.left = h; 
		x.color = x.left.color; 
		x.left.color = RED; 
		x.size = h.size; 
		h.size = size(h.left) + size(h.right) + 1; 
		return x; 
	}
	private void flipColors(NodoRB h){
		h.color = !h.color; 
		h.left.color = !h.left.color; 
		h.right.color = !h.right.color; 
	}
	private NodoRB moveRedLeft(NodoRB h){
		flipColors(h);
		if(isRed(h.right.left)){
			h.right = rotateRight(h.right); 
			h = rotateLeft(h); 
			flipColors(h);
		}
		return h; 
	}
	private NodoRB moveRedRight(NodoRB h){
		flipColors(h);
		if(isRed(h.left.left)){
			h = rotateRight(h); 
			flipColors(h);
		}
		return h; 
	}
	private NodoRB balance(NodoRB h){
		if(isRed(h.right)) h = rotateLeft(h); 
		if(isRed(h.left) && isRed(h.left.left)) h = rotateRight(h); 
		if(isRed(h.left) && isRed(h.right)) flipColors(h);
		h.size = size(h.left) + size(h.right) + 1; 
		return h; 
	}
	/**
	 * ------------------Metodos Utiles-----------------------------------
	 */
	public int altura(){
		return altura(root); 
	}
	private int altura(NodoRB x){
		if(x == null) return -1; 
		return 1 + Math.max(altura(x.left), altura(x.right)); 
	}

	public Key min(){
		if(isEmpty()) throw new NoSuchElementException("esta vacio"); 
		return min(root).key; 
	}
	private NodoRB min(NodoRB x){
		if(x.left == null) return x; 
		else return min(x.left); 
	}
	public Key max() {
		if (isEmpty()) throw new NoSuchElementException("called max() with empty symbol table");
		return max(root).key;
	} 
	private NodoRB max(NodoRB x) { 
		if (x.right == null) return x; 
		else                 return max(x.right); 
	} 
	public Key floor(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to floor() is null");
		if (isEmpty()) throw new NoSuchElementException("called floor() with empty symbol table");
		NodoRB x = floor(root, key);
		if (x == null) return null;
		else           return x.key;
	}    
	private NodoRB floor(NodoRB x, Key key) {
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp < 0)  return floor(x.left, key);
		NodoRB t = floor(x.right, key);
		if (t != null) return t; 
		else           return x;
	}
	public Key ceiling(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to ceiling() is null");
		if (isEmpty()) throw new NoSuchElementException("called ceiling() with empty symbol table");
		NodoRB x = ceiling(root, key);
		if (x == null) return null;
		else           return x.key;  
	}
	private NodoRB ceiling(NodoRB x, Key key) {  
		if (x == null) return null;
		int cmp = key.compareTo(x.key);
		if (cmp == 0) return x;
		if (cmp > 0)  return ceiling(x.right, key);
		NodoRB t = ceiling(x.left, key);
		if (t != null) return t; 
		else           return x;
	}
	public Key select(int k) {
		if (k < 0 || k >= size()) {
			throw new IllegalArgumentException("called select() with invalid argument: " + k);
		}
		NodoRB x = select(root, k);
		return x.key;
	}
	private NodoRB select(NodoRB x, int k) {
		int t = size(x.left); 
		if      (t > k) return select(x.left,  k); 
		else if (t < k) return select(x.right, k-t-1); 
		else            return x; 
	} 
	public int rank(Key key) {
		if (key == null) throw new IllegalArgumentException("argument to rank() is null");
		return rank(key, root);
	} 
	private int rank(Key key, NodoRB x) {
		if (x == null) return 0; 
		int cmp = key.compareTo(x.key); 
		if      (cmp < 0) return rank(key, x.left); 
		else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
		else              return size(x.left); 
	} 
	public Iterable<Key> keys() {
		if (isEmpty()) return (Iterable<Key>) new Queue<Key>();
		return keys(min(), max());
	}
	public Iterable<Key> keys(Key lo, Key hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to keys() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to keys() is null");

		Queue<Key> queue = new Queue<Key>();
		// if (isEmpty() || lo.compareTo(hi) > 0) return queue;
		keys(root, queue, lo, hi);
		return (Iterable<Key>) queue;
	} 
	private void keys(NodoRB x, Queue<Key> queue, Key lo, Key hi) { 
		if (x == null) return; 
		int cmplo = lo.compareTo(x.key); 
		int cmphi = hi.compareTo(x.key); 
		if (cmplo < 0) keys(x.left, queue, lo, hi); 
		if (cmplo <= 0 && cmphi >= 0) queue.enqueue(x.key); 
		if (cmphi > 0) keys(x.right, queue, lo, hi); 
	} 
	public int size(Key lo, Key hi) {
		if (lo == null) throw new IllegalArgumentException("first argument to size() is null");
		if (hi == null) throw new IllegalArgumentException("second argument to size() is null");

		if (lo.compareTo(hi) > 0) return 0;
		if (contains(hi)) return rank(hi) - rank(lo) + 1;
		else              return rank(hi) - rank(lo);
	}


}
