package model.t7;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;

import model.data_structures.BST;
import model.data_structures.RedBlackBST;
import model.vo.VOInfoPelicula;

public class Taller7 {

	private static BST arbol; 
	
	public static void cargar(){
		Gson g  = new Gson(); 
		BufferedReader br = null; 
		 
		try {
			br = new BufferedReader(new FileReader("links_json.json"));
			VOInfoPelicula[] resultado = g.fromJson(br, VOInfoPelicula[].class); 
			for(int i = 0; i< resultado.length ; i++){
				VOInfoPelicula actual = resultado[i]; 
				VOInfoPelicula siguiente= resultado[i++];
				if(actual == siguiente){
					RedBlackBST rb = new RedBlackBST<>();
					while(actual == siguiente){
						rb.put(actual.getMovieId(), actual.getImdbData().getTitle());
						actual = siguiente; 
					}
					arbol.put((Comparable) rb, rb.altura());
				}
				else{
					arbol.put(actual.getImdbData().getYear(), actual.getImdbData().getTitle());
				}
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	public static void main(String[] args) {
		cargar(); 
		arbol.print(arbol.daRaiz()); 
	}

}
