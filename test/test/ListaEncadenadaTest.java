package test;

import java.util.Iterator;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class ListaEncadenadaTest extends TestCase
{
	private ListaEncadenada<String> lista1;
	private Iterator<String> iterador;
	
	private void setupEscenario1()
	{
		try
		{
			lista1 = new ListaEncadenada();
		}
		catch(Exception e)
		{
			fail("No deberia lanzar excepci�n");
		}
	}
	
	private void setupEscenario2()
	{
		try
		{
			lista1 = new ListaEncadenada();
			lista1.agregarElementoFinal("Elemento A");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario3()
	{
		try
		{
			lista1 = new ListaEncadenada();
			lista1.agregarElementoFinal("Elemento A");
			lista1.agregarElementoFinal("Elemento B");
			lista1.agregarElementoFinal("Elemento C");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	private void setupEscenario4()
	{
		try
		{
			lista1 = new ListaEncadenada();
			lista1.agregarElementoFinal("Elemento A");
			lista1.agregarElementoFinal("Elemento B");
			lista1.agregarElementoFinal("Elemento C");
			lista1.agregarElementoFinal("Elemento D");
			lista1.agregarElementoFinal("Elemento E");
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar excepci�n");
		}
	}
	
	public void testIterator()
	{
		setupEscenario2();
		iterador = lista1.iterator();
		assertTrue("Deber�a ser falso", iterador.hasNext());
		setupEscenario3();
		iterador = lista1.iterator();
		assertTrue("Deber�a ser verdadero", iterador.hasNext());
		assertEquals("Deber�a ser elemento A", "Elemento A", iterador.next());
	}
	
	public void testAgregarElementoFinal()
	{
		setupEscenario1();
		lista1.agregarElementoFinal("A");
		lista1.agregarElementoFinal("B");
		lista1.agregarElementoFinal("C");
		assertEquals("A", lista1.darElemento(0));
		assertEquals("B", lista1.darElemento(1));
		assertEquals("C", lista1.darElemento(2));
	}
	
	public void testDarElemento()
	{
		setupEscenario3();
		assertEquals("Elemento A", lista1.darElemento(0));
		assertEquals("Elemento B", lista1.darElemento(1));
		assertEquals("Elemento C", lista1.darElemento(2));
	}
	
	public void testDarNumeroElementos()
	{
		setupEscenario4();
		assertEquals(5, lista1.darNumeroElementos());
	}
	
	public void testDarElementoPosicionActual()
	{
		setupEscenario1();
		assertNull("No deber�a retornar", lista1.darElementoPosicionActual());
		setupEscenario3();
		assertEquals("Deber�a retornar elemento C", "Elemento C", lista1.darElementoPosicionActual());
	}
	
	public void testRetrocederPosicionAnterior()
	{
		setupEscenario1();
		assertFalse("No deber�a retroceder", lista1.retrocederPosicionAnterior());
		lista1.agregarElementoFinal("Elemento B");
		assertFalse("No deber�a retroceder", lista1.retrocederPosicionAnterior());
		setupEscenario3();
		assertTrue("Deber�a retroceder una posici�n", lista1.retrocederPosicionAnterior());
	}
	
}
